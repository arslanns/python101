class Account:
    def __init__(self):
        self.balance = 0

    def deposit(self, amount):
        self.balance += amount

    def withdraw(self, amount):
        if self.balance < amount:
            raise ValueError("Not enough balance")
        self.balance -= amount

    def pay_interest(self, interest):
        self.balance *= interest

    def transfer(self, other, amount):
        self.withdraw(amount)
        other.deposit(amount)


barclays = Account()
halifax = Account()
print(f"My balance is {barclays.balance}")

barclays.deposit(100)
print(f"My balance is {barclays.balance}")

barclays.deposit(100)
print(f"My balance is {barclays.balance}")

barclays.pay_interest(1.3)
print(f"My balance is {barclays.balance}")

barclays.transfer(halifax, 100)
print(f"My balance is {barclays.balance}")
print(f"My balance is {halifax.balance}")