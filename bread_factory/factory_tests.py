# Import function & run tests 
from factory_functions import *
from factory import *

import unittest


class TestBakingFunctions(unittest.TestCase):                                                                      
# **User Story 1** AS a bread maker, I want to provide `flour` and `water` to my `make_dough_option` and get out `dough`, else I want `not dough`. So that I can then bake the bread.
    def test_make_dough(self):
        self.assertEqual( make_dough_option('flour', 'water'), 'dough')
    def test_make_not_dough(self):
        self.assertEqual( make_dough_option('cement', 'water'), 'not dough')

# **User Story 2** AS a bread maker, I want to provide `dough` to my `bake_option` and get out `Pao`, else I want `not Pao`. So that I can then eat the bread.
    def test_bake(self):
        self.assertEqual( bake_option('dough'), 'Pao')
    def test_not_bake(self):
        self.assertEqual( bake_option('not dough'), 'not Pao')
        
# **User Story 3** AS a bread maker, I want to provide `flour` and 'water' to my `run_factory` and get out `Pao`, else I want `not Pao`. So that I can then eat the bread.
    def test_factory(self):
        self.assertEqual(run_factory('water', 'flour'), 'Pao')
    def test_not_factory(self):
        self.assertEqual(run_factory('water', 'notflour'), 'not Pao')

if __name__ == '__main__':
    unittest.main()
