#function to take in flour and water to make dough, otherwise return not dough
def make_dough_option(arg1, arg2):
    if ('flour' == arg1 or 'flour' == arg2) and ('water' == arg1 or 'water' == arg2):
        return 'dough'
    else:
        return 'not dough'

#function to take in dough and return Pao, otherwise return not Pao
def bake_option(arg1):
    if 'dough' == arg1:
        return 'Pao'
    else:
        return 'not Pao'

#uses both functions from factory_function to take in water and flour and return Pao otherwise return not Pao
def run_factory(arg1, arg2):
    return bake_option( make_dough_option(arg1, arg2))
