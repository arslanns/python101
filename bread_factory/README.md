# Pao Factory TDD project 🥖

This project is to explore the coding of a bread factory (pão). 

We will cover topics including: 

- git, bitbucket
- Markdown
- Agile / scrum principles
- TDD (Test Driven Development)
- Unit tests
- Functional programing


Other principles:

- Sepration of concerns
- Dry code

### TDD & Unit tests

**Unit test**, is a single test that tests a part of a function.
Several tests together will help ensure the functionality of the program, reduce technical debt and keep it maintainable.

Generally code lives, entropy adds complexity and short cuts lead to technical debt that can kill a project.

Technical debt is the concept of taking shortcuts - like skipping documentation or not making unite tests - leading to unmanageble code. Other factors such a time, people leaving, updates also add to technical debt.

**Test Driven Development** Is a way of developing code that is very Agile :) It's the lightes implementation of Agile - Ensures working code. 

You make the test firs, then you code the least amount of code to make the test pass.

[TDD](https://www.google.com/url?sa=i&url=https%3A%2F%2Fmedium.com%2Fcreditas-tech%2Fo-que-test-driven-development-n%25C3%25A3o-%25C3%25A9-584af2d4c65a&psig=AOvVaw07kGOdBLVSsAJDUDdLXRNi&ust=1643211972684000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCPDY_OGfzfUCFQAAAAAdAAAAABAD)

### User Stories and tests

Good User Stories can be used to make you unit tests. These are usually done by the `Three Amigos` `Devs`+ `testers`+ `business analyist`.

In our case it will just be us.

**User Story 1** AS a bread maker, I want to provide `flour` and `water` to my `make_dough_option` and get out `dough`, else I want `not dough`. So that I can then bake the bread.

**User Story 2** AS a bread maker, I want to be able able take `dough` and user the `bake_option` to get `Pao`. Else I should get `not Pao`. So That I can make bread.

**User Story 3** As a break maker, I want an option of `run_factory` that will take in `flour` and `water` and give me `Pao`, else give me `not Pao`. So I can make break with one simple action.