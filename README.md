# Python 101 

Python is a programing language that is syntax light and easy to pick up. It is very versatile and well paid. Used across the industry for:

- simple scripts
- Automation (ansibles)
- Web Development ()
- Data and ML
- Others

List of things to cover:

- Data types
- Strings
- Numericals
- Booleans
- List
- If conditions
- Logical Or and logical and
- Dictionaties
- Loops
- While loops
- Embed lists and dictionaries
- Debugers
- Functions
- TDD & Unitesting
- Error handling
- External packages
- API's


Other topics to talk:

- Polymorphsim
- TDD
- DRY code
- Seperation of concerns
- ETL - Extract transform and load
- Frameworks (difference to actual languages)


## Python Intro a basics

Python you can make files and run them, you can run on the command line, you can have a Framework running python or use an IDE.

To try stuff out super quick, use the comand line.

```bash 
python3
>>>>
```

Then you can write python.

```python
>>> human = "Mike Page"
>>> print(human)
Mike Page

```


