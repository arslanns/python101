l = [1,2,3,4]

try:
    print(f"The item at index 1 is {l[1]}")
    print(l[1] + '2')
except IndexError:
    print("an error happened but I can ignore it")
except TypeError:
    print('type error')
else:
    print("no exceptions raised")

#raise ValueError("how to raise an exception manually")

print("All done")

